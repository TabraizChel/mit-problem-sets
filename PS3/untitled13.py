#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 11:46:22 2018

@author: chelt
"""

def update_hand(hand, word):
    word  = word.lower()
    hand_update = hand.copy()
    for letter in word:
        if letter in hand_update.keys():
                hand_update[letter] -= 1
                
    return hand_update


def is_valid_word(word, hand):
    """
    
    hand_update = hand.copy()
    print(hand_update)
    word  = word.lower()
    if word in word_list:
        for letter in word:
            if not (letter in hand_update.keys() and hand_update[letter] > 0):
                return False
            else:
                    hand_update = update_hand(hand_update, letter)
    return True
    
    
    Returns True if word is in the word_list and is entirely
    composed of letters in the hand. Otherwise, returns False.
    Does not mutate hand or word_list.
   
    word: string
    hand: dictionary (string -> int)
    word_list: list of lowercase strings
    returns: boolean
    """
    hand_update = hand.copy()
    print(hand_update)
    word  = word.lower()
    word1 = word.copy
    vowel = 'aeiou'        
    if '*' in word:
        for letter in vowel:
            word1.replace('*', letter)
            if word1 in word_list:
                break
            else:
                return False 
        
        
    for letter in word:
            if not (letter in hand_update.keys() and hand_update[letter] > 0):
                return False
            else:
                hand_update = update_hand(hand_update, letter)
                
    return True
word = "hello"
hand  = {'h':1, 'e': 1, 'l': 2, '*': 1}
print(is_valid_word(word, hand))

