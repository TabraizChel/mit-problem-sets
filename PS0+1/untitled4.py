#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 12:22:16 2018

@author: chelt
"""
annual_salary = float(input('Enter annual salary: '))
portion_saved =float(input('Enter the percent of your salary to save, as a decimal: '))
total_cost = float(input('Enter the cost of your dream home: '))
portion_down_payment = 0.25*total_cost
current_savings = 0
r = 0.04
month_calculator = 0
while current_savings < portion_down_payment:
    current_savings += current_savings*(r/12)
    current_savings += portion_saved*(annual_salary/12)
    month_calculator +=1
    
print('Number of months: ',month_calculator)
    