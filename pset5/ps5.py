# 6.0001/6.00 Problem Set 5 - RSS Feed Filter
# Name:
# Collaborators:
# Time:

import feedparser
import string
import time
import threading
from project_util import translate_html
from mtTkinter import *
from datetime import datetime
import pytz


#-----------------------------------------------------------------------

#======================
# Code for retrieving and parsing
# Google and Yahoo News feeds
# Do not change this code
#======================

def process(url):
    """
    Fetches news items from the rss url and parses them.
    Returns a list of NewsStory-s.
    """
    feed = feedparser.parse(url)
    entries = feed.entries
    ret = []
    for entry in entries:
        guid = entry.guid
        title = translate_html(entry.title)
        link = entry.link
        description = translate_html(entry.description)
        pubdate = translate_html(entry.published)

        try:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %Z")
            pubdate.replace(tzinfo=pytz.timezone("GMT"))
          #  pubdate = pubdate.astimezone(pytz.timezone('EST'))
          #  pubdate.replace(tzinfo=None)
        except ValueError:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %z")

        newsStory = NewsStory(guid, title, description, link, pubdate)
        ret.append(newsStory)
    return ret

#======================
# Data structure design
#======================

# Problem 1

# TODO: NewsStory

class NewsStory():
    def __init__(self,guid,title,description,link,pubdate):
        self.guid = guid
        self.title = title
        self.description = description
        self.link = link
        self.pubdate = pubdate
        
    def get_guid(self):
        return self.guid
    
    def get_title(self):
        return self.title
    
    def get_description(self):
        return self.description
    
    def get_link(self):
        return self.link
    
    def get_pubdate(self):
        return self.pubdate
    
#======================
# Triggers
#======================

class Trigger(object):
    def evaluate(self, story):
        """
        Returns True if an alert should be generated
        for the given news item, or False otherwise.
        """
        # DO NOT CHANGE THIS!
        raise NotImplementedError

# PHRASE TRIGGERS

# Problem 2
# TODO: PhraseTrigger

class PhraseTrigger(Trigger):
    
    def __init__(self,phrase):
        Trigger.__init__(object)
        self.phrase = phrase.lower()
    
    def is_phrase_in(self, text):
        text = text.lower()
        for character in text:
            if character in string.punctuation:
                text = text.replace(character,' ')      
        text = ' '.join(text.split())
        return (' ' + self.phrase + ' ') in (' ' + text + ' ')
        
        
        
        
        
    

# Problem 3
# TODO: TitleTrigger
class TitleTrigger(PhraseTrigger):
    def __init__(self,phrase):
        PhraseTrigger.__init__(self,phrase)
    
    def evaluate(self, Title):
        return self.is_phrase_in(Title.title)
    
    
# Problem 4
# TODO: DescriptionTrigger
class DescriptionTrigger(PhraseTrigger):
    def __init__(self,phrase):
        PhraseTrigger.__init__(self,phrase)    
    def evaluate(self, Description):
        return self.is_phrase_in(Description.description)
    
        
# TIME TRIGGERS

# Problem 5
# TODO: TimeTrigger
# Constructor:
#        Input: Time has to be in EST and in the format of "%d %b %Y %H:%M:%S".
#        Convert time from string to a datetime before saving it as an attribute.
        
class TimeTrigger(Trigger):
    def __init__(self,time):
        Trigger.__init__(object)
        time = datetime.strptime(time,'%d %b %Y %H:%M:%S')
        time = time.replace(tzinfo=pytz.timezone('EST'))
        self.time = time 

# Problem 6
# TODO: BeforeTrigger and AfterTrigger
class BeforeTrigger(TimeTrigger):
    
    def __init__(self,time):
        TimeTrigger.__init__(self,time)
        
    def evaluate(self,Story):
        Pubdate = Story.get_pubdate()
        Pubdate = Pubdate.replace(tzinfo=pytz.timezone('EST'))
        if Pubdate < self.time:
            return True
        
class AfterTrigger(TimeTrigger):
    
    def __init__(self,time):
        TimeTrigger.__init__(self,time)
        
    def evaluate(self,Story):
        Pubdate = Story.get_pubdate()
        Pubdate = Pubdate.replace(tzinfo=pytz.timezone('EST'))
        if Pubdate > self.time:
            return True
        

# COMPOSITE TRIGGERS

# Problem 7
# TODO: NotTrigger
class NotTrigger(Trigger):
    def __init__(self, trigger):
        Trigger.__init__(object)
        self.trigger = trigger
        
    def evaluate (self, newsitem):
        return not self.trigger.evaluate(newsitem)
        
# Problem 8
# TODO: AndTrigger
class AndTrigger(Trigger):
    def __init__(self, trigger1, trigger2):
        Trigger.__init__(object)
        self.trigger1 = trigger1
        self.trigger2 = trigger2
        
        
    def evaluate(self, newsitem):
        
        return (self.trigger1.evaluate(newsitem) and self.trigger2.evaluate(newsitem))

        
# Problem 9
# TODO: OrTrigger
class OrTrigger(Trigger):
    def __init__(self, trigger1, trigger2):
        Trigger.__init__(object)
        self.trigger1 = trigger1
        self.trigger2 = trigger2
        
        
    def evaluate(self, newsitem):
        
        return (self.trigger1.evaluate(newsitem) or self.trigger2.evaluate(newsitem))

#======================
# Filtering
#======================

# Problem 10
def filter_stories(stories, triggerlist):
    """
    Takes in a list of NewsStory instances.

    Returns: a list of only the stories for which a trigger in triggerlist fires.
    """
    matching_stories = []
    for story in stories:
        for trigger in triggerlist:
            if trigger.evaluate(story):
                matching_stories.append(story)
                break
                
                
    
    # TODO: Problem 10
    # This is a placeholder
    # (we're just returning all the stories, with no filtering)
    return matching_stories



#======================
# User-Specified Triggers
#======================
# Problem 11
def read_trigger_config(filename):
    """
    filename: the name of a trigger configuration file

    Returns: a list of trigger objects specified by the trigger configuration
        file.
    """
    # We give you the code to read in the file and eliminate blank lines and
    # comments. You don't need to know how it works for now!
    
    def create_TitleTrigger(*kwargs):
        trigger = TitleTrigger(*kwargs)
        return trigger
    
    def create_DescriptionTrigger(*kwargs):
        trigger = PhraseTrigger(*kwargs)
        return trigger
    
    def create_BeforeTrigger(*kwargs):
        trigger = BeforeTrigger(*kwargs)
        return trigger
    
    def create_AfterTrigger(*kwargs):
        trigger = AfterTrigger(*kwargs)
        return trigger
    
    def create_NotTrigger(*kwargs):
        trigger = NotTrigger(*kwargs)
        return trigger
    
    def create_AndTrigger(*kwargs):
        trigger = AndTrigger(*kwargs)
        return trigger
    
    def create_OrTrigger(*kwargs):
        trigger = OrTrigger(*kwargs)
        return trigger
    
    
    trigger_dict = {'TITLE':create_TitleTrigger, 'DESCRIPTION':create_DescriptionTrigger,'AFTER':create_AfterTrigger,
                    'BEFORE':create_BeforeTrigger, 'NOT':create_NotTrigger, 'AND':create_AndTrigger,'OR':create_OrTrigger}
    
    
    
    trigger_file = open(filename, 'r')
    lines = []
    for line in trigger_file:
        line = line.rstrip()
        if not (len(line) == 0 or line.startswith('//')):
            lines.append(line)
    
    trigger_temp = {} 
    trigger_list = []
    for sentence in lines:
        try:
            l = sentence.split(',')
            rest = tuple(l[2:])
            trigger_holder = trigger_dict[l[2]](rest)
            trigger_temp['t'+str(l[0][1])] = trigger_holder
        except NameError:
            l = sentence.split(',')
            rest = l[1:]
            for triggers in rest:
                trigger_list.append(trigger_temp[triggers])
    return trigger_list
                
            
            
        
    # TODO: Problem 11
    # line is the list of lines that you need to parse and for which you need
    # to build triggers
    
    print(lines) # for now, print it so you see what it contains!



SLEEPTIME = 120 #seconds -- how often we poll

def main_thread(master):
    # A sample trigger list - you might need to change the phrases to correspond
    # to what is currently in the news
    try:
        t1 = TitleTrigger("election")
        t2 = DescriptionTrigger("Trump")
        t3 = DescriptionTrigger("Clinton")
        t4 = AndTrigger(t2, t3)
        triggerlist = [t1, t4]

        # Problem 11
        # TODO: After implementing read_trigger_config, uncomment this line 
        triggerlist = read_trigger_config('triggers.txt')
        
        # HELPER CODE - you don't need to understand this!
        # Draws the popup window that displays the filtered stories
        # Retrieves and filters the stories from the RSS feeds
        frame = Frame(master)
        frame.pack(side=BOTTOM)
        scrollbar = Scrollbar(master)
        scrollbar.pack(side=RIGHT,fill=Y)

        t = "Google & Yahoo Top News"
        title = StringVar()
        title.set(t)
        ttl = Label(master, textvariable=title, font=("Helvetica", 18))
        ttl.pack(side=TOP)
        cont = Text(master, font=("Helvetica",14), yscrollcommand=scrollbar.set)
        cont.pack(side=BOTTOM)
        cont.tag_config("title", justify='center')
        button = Button(frame, text="Exit", command=root.destroy)
        button.pack(side=BOTTOM)
        guidShown = []
        def get_cont(newstory):
            if newstory.get_guid() not in guidShown:
                cont.insert(END, newstory.get_title()+"\n", "title")
                cont.insert(END, "\n---------------------------------------------------------------\n", "title")
                cont.insert(END, newstory.get_description())
                cont.insert(END, "\n*********************************************************************\n", "title")
                guidShown.append(newstory.get_guid())

        while True:

            print("Polling . . .", end=' ')
            # Get stories from Google's Top Stories RSS news feed
            stories = process("http://news.google.com/news?output=rss")

            # Get stories from Yahoo's Top Stories RSS news feed
            stories.extend(process("http://news.yahoo.com/rss/topstories"))

            stories = filter_stories(stories, triggerlist)

            list(map(get_cont, stories))
            scrollbar.config(command=cont.yview)


            print("Sleeping...")
            time.sleep(SLEEPTIME)

    except Exception as e:
        print(e)


if __name__ == '__main__':
    root = Tk()
    root.title("Some RSS parser")
    t = threading.Thread(target=main_thread, args=(root,))
    t.start()
    root.mainloop()

