# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

master_list = []

def get_permutations(sequence):
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute. Assume that it is a
    non-empty string.  

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    >>> get_permutations('abc')
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''
    
    # turn string in list
    def passthrough(holder, string):
        ''' pass holder through each point in string'''
    
        def stringtoList(string):
            string_list = []
            for char in string:
                string_list.append(char)
            return string_list
        
        def listtoString(List):
            return ''.join(List)
        
        returnList = []
        for word in string:  
            for i in range(len(word) + 1):
                L = stringtoList(word)
                L.insert(i,holder)
                returnList.append(listtoString(L))
        return returnList
    
    
    if len(sequence) == 1:
        return list(sequence)
    
    if len(sequence) > 1:
        return passthrough(sequence[:1] , get_permutations(sequence[1:]))
    
        
    
    
    
    
    
    
        
    
    

    pass #delete this line and replace with your code here

if __name__ == '__main__':
#    #EXAMPLE
#    example_input = 'abc'
#    print('Input:', example_input)
#    print('Expected Output:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
#    print('Actual Output:', get_permutations(example_input))
    
#    # Put three example test cases here (for your sanity, limit your inputs
#    to be three characters or fewer as you will have n! permutations for a 
#    sequence of length n)

    pass #delete this line and replace with your code here

