#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 13:04:10 2018

@author: chelt
"""

import string



string1 = 'hi my name is'
string2  = 'bye my name is'


                
        

        
        
    
    
    
    
    
    
def get_permutations(sequence):
    
    def passthrough(holder, string):
    
        def stringtoList(string):
            string_list = []
            for char in string:
                string_list.append(char)
            return string_list
        
        def listtoString(List):
            return ''.join(List)
        
        returnList = []
        
         
        for word in string:  
            for i in range(len(word) + 1):
                L = stringtoList(word)
                L.insert(i,holder)
                returnList.append(listtoString(L))
        return returnList
    
    
    if len(sequence) == 1:
        return list(sequence)
    
    if len(sequence) > 1:
        return passthrough(sequence[:1] , get_permutations(sequence[1:]))
    
    

def build_shift_dict(shift): 
        '''
        Creates a dictionary that can be used to apply a cipher to a letter.
        The dictionary maps every uppercase and lowercase letter to a
        character shifted down the alphabet by the input shift. The dictionary
        should have 52 keys of all the uppercase letters and all the lowercase
        letters only.        
        
        shift (integer): the amount by which to shift every letter of the 
        alphabet. 0 <= shift < 26

        Returns: a dictionary mapping a letter (string) to 
                 another letter (string). 
        '''
        dict_lowercase = {}
        String_lowercase = string.ascii_lowercase
        for i in range(len(String_lowercase)):
            if i + shift > 25:
                dict_lowercase[String_lowercase[i]] = String_lowercase[(i + shift)-26]
            else:
                
                dict_lowercase[String_lowercase[i]] = String_lowercase[i + shift]
        
        dict_uppercase = {}
        String_uppercase = string.ascii_uppercase
        for i in range(len(String_uppercase)):
            if i + shift > 25:
                dict_uppercase[String_uppercase[i]] = String_uppercase[(i + shift)-26]
            else:
                
                dict_uppercase[String_uppercase[i]] = String_uppercase[i + shift]
                
        dict_uppercase[' '] = ' '
        return {**dict_lowercase , **dict_uppercase}

def apply_shift(shift):
        '''
        Applies the Caesar Cipher to self.message_text with the input shift.
        Creates a new string that is self.message_text shifted down the
        alphabet by some number of characters determined by the input shift        
        
        shift (integer): the shift with which to encrypt the message.
        0 <= shift < 26

        Returns: the message text (string) in which every character is shifted
             down the alphabet by the input shift
        '''
        def stringtoList(string):
            string_list = []
            for char in string:
                string_list.append(char)
            return string_list
        
        def listtoString(List):
            return ''.join(List)
        
        cipher = build_shift_dict(shift)
        text = 'himynameis tab'
        cipherText = ''
        for letter in text:
            cipherText += cipher[letter]
        return cipherText    
        

print(get_permutations('aeiou'))
    
