# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    check = 0
    for letter in secret_word:
        if letter in letters_guessed:
            check += 1
    if check == len(secret_word):
        return True
    else:
        return False
            
    



def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    guess = ''
    for letter in secret_word:
        if letter in letters_guessed:
            guess += letter
        else: 
            guess += ' _ '
    return guess
    



def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    alphabet = string.ascii_lowercase
    alphabetList = list(alphabet)
    letters_left  = ''
    for letters in letters_guessed:
        if letters in alphabet:
            alphabetList.remove(letters)
    for letters in  alphabetList:
        letters_left += letters        
    return letters_left

def warning_check(warnings):
    if warnings <= 0:
        return True
    
def vowel_check(letter):
    if letter in 'aeiou':
        return True
    
def letter_in_word(user_guess, secret_word):
    if user_guess not in secret_word:
        print(user_guess,'not in word')
        return False
def unique_letter_check(secret_word):
    secret_word = set(secret_word)
    return len(secret_word)

def total_score(guesses,secret_word):
    return unique_letter_check(secret_word)*guesses         
    
    

def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    guesses = 6
    warnings = 3
    letters_guessed = []
    print('my word is ', len(secret_word)  ,' long.') 
    while True:
        print('guesses: ', guesses)
        print('letters remaining: ', get_available_letters(letters_guessed))
         
        while True:
            user_guess = input('please guess a letter: ')
            user_guess  = user_guess.lower()
            if user_guess in string.ascii_lowercase and len(user_guess) == 1:
                letters_guessed.append(user_guess)
                break
            else:
                warnings = warnings - 1
                if warning_check(warnings) == True:
                    print('invalid guess, please enter a single letter: ')
                    print('warnings: ' , 0, ' you will now lose guesses')
                    guesses -= 1
                    continue
                print('invalid guess, please enter a single letter: ')
                print('warnings: ' , warnings)
    
        
        if letter_in_word(user_guess, secret_word) == False:
            if vowel_check(user_guess):
                guesses -= 2
            else:
                guesses -= 1
            
            
        
        print(get_guessed_word(secret_word, letters_guessed))
        
        if is_word_guessed(secret_word, letters_guessed):
            print('Congrats you won !!!')
            print('total score: ' , total_score(guesses,secret_word))
            
            break
        if guesses <= 0:
            print('You lost ... Game Over.', 'The word was: ', secret_word)
            break



# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test
#(hint: you might want to pick your own
# secret_word while you're doing your own testing)


# -----------------------------------



def match_with_gaps(my_word, other_word):
    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    for letter,char in zip(my_word.replace(" ", ""),other_word):
        if len(my_word.replace(" ", "")) != len(other_word):
            return False
        if letter == '_':
            if char in my_word:
                return False
            else:
                continue            
        elif letter != char:
            return False       
    return True 



def show_possible_matches(my_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    print('showing possible matches for: ', my_word)
    string = ''
    for other_word in wordlist:
        if match_with_gaps(my_word, other_word) == True:
            string += other_word + ' '
    if string != '':
        print(string)
    else:
        print('no matches found')



def hangman_with_hints(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses
    
    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter
      
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
      
    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word. 
    
    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    letters_guessed = []
    guesses = 6
    warnings = 3
    print('my word is ', len(secret_word)  ,' long.') 
    while True:
        print('guesses: ', guesses)
        print('letters remaining: ', get_available_letters(letters_guessed))
         
        while True:
            user_guess = input('please guess a letter: ')
            user_guess  = user_guess.lower()
            if user_guess in string.ascii_lowercase and len(user_guess) == 1:
                letters_guessed.append(user_guess)
                break
            
            elif user_guess == '*':
                show_possible_matches(get_guessed_word(secret_word, letters_guessed))
                
            else:
                warnings = warnings - 1
                if warning_check(warnings) == True:
                    print('invalid guess, please enter a single letter: ')
                    print('warnings: ' , 0, ' you will now lose guesses')
                    guesses -= 1
                    continue
                print('invalid guess, please enter a single letter: ')
                print('warnings: ' , warnings)
    
        
        if letter_in_word(user_guess, secret_word) == False:
            if vowel_check(user_guess):
                guesses -= 2
            else:
                guesses -= 1
            
            
        
        print(get_guessed_word(secret_word, letters_guessed))
        
        if is_word_guessed(secret_word, letters_guessed):
            print('Congrats you won !!!')
            print('total score: ' , total_score(guesses,secret_word))
            break
        if guesses <= 0:
            print('You lost ... Game Over')
            break



# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


if __name__ == "__main__":
    # pass

    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.
    
    #secret_word = choose_word(wordlist)
    #hangman(secret_word)

###############
    
    # To test part 3 re-comment out the above lines and 
    # uncomment the following two lines. 
    
    secret_word = choose_word(wordlist)
    hangman_with_hints(secret_word)
